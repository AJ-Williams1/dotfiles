# Setup history
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.cache/zsh/history

# Make prompt pretty
PS1='%B%F{240}[%f%F{#7B00C7}%n%f%F{22}@%f%F{26}%m%f%F{88} %~%f%F{240}]%f%b%F{184} %#%f%F{white}%f '

# Autocd
setopt autocd

# not Vim mode
bindkey -e

# Variables
export PATH="$PATH:/home/aj/.scripts:/home/aj/.local/bin"
export PASSWORD_STORE_GENERATED_LENGTH="128"
export READER="zathura"
export EDITOR="vim"
export PASTEL_COLOR_MODE=24bit
CC="clang"
CFLAGS="-fsanitize=signed-integer-overflow -fsanitize=undefined -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow"
LDLIBS="-lcrypt -lcs50 -lm"
export PATH='/bin:/usr/local/sbin:/usr/local/bin:/usr/bin/usr/bin/site_perl/usr/bin/vendor_perl/usr/bin/core_perl:/home/aj/.scripts:/home/aj/.scripts:/home/aj/.scripts:/home/aj/.local/bin:/home/aj/.scripts:/home/aj/.local/bin:/home/aj/.cargo/bin'

# Aliases
alias tmux="tmux -uf ~/.config/tmux.conf"
alias ls="ls -B --color --group-directories-first"
alias ctags="ctags -R --c-kinds=+p"
alias cmatrix="cmatrix -ab"
alias ed="ed -v -p\*"
alias roll="DISPLAY= mpv --quiet --vo=caca /home/aj/vids/roll.mp4"
alias ss="setsid -f"
alias vim='vim -c "let g:tty='\''$(tty)'\''"'
alias gotop='gotop -c monokai'
alias tsm='transmission-remote'
alias chpres='cd /home/aj/builds/suckless/sent && nvim config.h'

function pdfman(){
	man -Tpdf "$1" | zathura - &
}

############### Completion ###############

zmodload -i zsh/complist

# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-/'\''"`]=** r:|=**'
zstyle ':completion:*' menu select
zstyle ':completion:*' original false
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/aj/.config/zsh/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

zstyle ':completion:*' list-colors ''

# Use vim keys for completion menu navigation
bindkey -M menuselect '^o' accept-and-infer-next-history

# Syntax highlighting
source /home/aj/.scripts/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh
