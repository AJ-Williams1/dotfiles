set shiftwidth=4 tabstop=4 softtabstop=4 expandtab autoindent smartindent
setlocal colorcolumn=100

setlocal path=.,**,/usr/include
set include=^#include\ \\(\\"\\\|<\\)\\zs.\\+\.h\\ze\\(\\"\\\|>\\)
setlocal define=\\(^\\s*[^()\ ]\\+\\s\\+\\ze[^()\ ]\\+(.*)[^;]*$\\\|^\\s*}\\s*\\(__attribute__((.*))\\n^\\)\\?\\ze[^;]\\+;\\\|^\\s*\\(const\\s\\+[a-zA-Z0-9_]\\+\\s\\+\\\|[a-zA-Z0-9_]\\+\\s\\+const\\s\\+\\)\\)
" function defs: ^\s*[^() ]\+\s\+\ze[^() ]\+(.*)[^;]*$
" typdefs: ^\s*}\s*\(__attribute__((.*))\n^\)\?\ze[^;]\+;
" consts: ^\s*\(const\s\+[a-zA-Z0-9_]\+\s\+\|[a-zA-Z0-9_]\+\s\+const\s\+\)
compiler gcc
nnoremap <space>b :silent make <bar> redraw!<CR>
